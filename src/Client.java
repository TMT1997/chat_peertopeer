
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Talaat
 */
public class Client {
    Socket client;
    //Datagram socket is a type of network socket which provides connection-less point for sending and receiving packets
    private static DatagramSocket broadcast_socket = null;
    /*
    The class DataOutputStream allows you to write Java primitive data types;
    many of its methods write a single Java primitive type to the output stream.
    The method writeBytes is a useful one.
    */
    DataOutputStream output_data;
    int port = 8888;
    public static int broadcast_port = 4415;
    public Client(String target_ip){
        try
        {
            //simple tcp
            client = new Socket();
            client.connect(new InetSocketAddress(target_ip, port),200);
            //write data
            output_data = new DataOutputStream(client.getOutputStream());
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    //send data to the server
    public void send_data(String data){
        try{       
            output_data.writeUTF(data);//write string to the other connection
        }
        catch(Exception e){
            //System.out.println(e);
        }
    }
    public void send_file(String path) throws FileNotFoundException, IOException
    {
        File file = new File(path);
        InputStream in = new FileInputStream(file);
        // Get the size of the file
        long length = file.length();
        byte[] bytes = new byte[16 * 1024];
        
        int count;
        while ((count = in.read(bytes)) > 0) {
            output_data.write(bytes, 0, count);
        }
    }
    //------------------------------------
    public void close(){
       try{
            output_data.close();
            client.close();
        }
        catch(Exception e){
            //System.out.println(e);
        } 
    }
    //------------------------------------
    public static void broadcast(String broadcastMessage, InetAddress address ,String type) 
    {
        try
        {
            System.err.println("start broadcasting");
            broadcast_socket = new DatagramSocket();
            broadcast_socket.setBroadcast(true);
            
            byte[] buffer;
            if(type.equals("M"))
            {
                buffer = broadcastMessage.getBytes();
            }
            else
                buffer = read_file_broadcast(broadcastMessage);
            
            byte[] type_bytes = type.getBytes();
            
            DatagramPacket type_packet  = new DatagramPacket(type_bytes, type_bytes.length, address, broadcast_port);
            broadcast_socket.send(type_packet);
            DatagramPacket packet  = new DatagramPacket(buffer, buffer.length, address, broadcast_port);
            broadcast_socket.send(packet);
            broadcast_socket.close();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "here"+e);
        }
    }

    public static byte[] read_file_broadcast(String path) throws FileNotFoundException, IOException
    {
        File file = new File(path);
        InputStream in = new FileInputStream(file);
        // Get the size of the file
        long length = file.length();
        byte[] bytes = new byte[16 * 1024];
        
        int count;
        while ((count = in.read(bytes)) > 0);
        
        return bytes;
    }
    //------------------------------------
}
