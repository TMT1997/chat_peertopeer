
import java.awt.Color;
import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Talaat
 */
public class Server implements Runnable{
    /**
     *
     */
    ServerSocket server;
    Socket client;
    DataInputStream input_data;
    public static ArrayList<String> IPs = new ArrayList();
//    {"192.168.1.255" , "127.0.0.1" , "192.168.1.11" , "192.168.1.12"};
    int port = 8888;
    
    public Server()
    {
        try 
        {
            String broadcast_address="";
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) 
            {
                NetworkInterface networkInterface = interfaces.nextElement();
                
                if (networkInterface.isLoopback())
                    continue;    // Do not want to use the loopback interface.
                //virtual interface is not a physical interface like Fast Ethernet interface
                for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) 
                {
                    InetAddress broadcast = interfaceAddress.getBroadcast();
                    if (broadcast == null)
                        continue;
                    if(InetAddress.getByName(Server.get_ip()).equals(interfaceAddress.getAddress()))
                    {
                        broadcast_address = interfaceAddress.getBroadcast().toString();
                    }
                    // Do something with the address.
                }
            }
            IPs.add(broadcast_address.replaceFirst("/", ""));
            IPs.add("192.168.1.104");
            IPs.add("192.168.1.11");
            IPs.add("192.168.1.12");
//            Server.get_all_ips();
            //--------------------------------
            server = new ServerSocket(port);
        } 
        catch (IOException ex) 
        {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    public void listen()
    {
        try
        {
            client = server.accept();
            input_data = new DataInputStream(client.getInputStream());
        }
        catch(Exception e)
        {
            
        }
        
    }
    //read data from clients
     public String get_input(){
        String data = ""; 
        try{      
        data = input_data.readUTF();//read input as string
    }catch(Exception e){
        System.out.println(e);
    
        }// end exception
        return data;
    }
     public void close_server(){
       try{
            server.close();
        }
        catch(Exception e){
            System.out.println("Close ->"+e);
        } 
    }
     //func to get the current ip4v address
    public static String get_ip(){
        InetAddress address;
        try{
        address = InetAddress.getLocalHost();
        return address.getHostAddress(); // return the address
        }
        catch(Exception e){
            System.out.println("get ip ->"+e);
            return "False";
        }
    }
    
    // it take a lot of time to ping on 255 device in the network
    public static void get_all_ips(){
 
        String myip = Server.get_ip();
        
        if(myip.equals("127.0.0.1")){
            JOptionPane.showMessageDialog(null, "You must connect to any lan first");
        }
        else {
            String mynetworkips=new String(); // just a new string to store currently scanning ip

            for(int i=myip.length()-1;i>=0;--i){
                if(myip.charAt(i)=='.'){
                    mynetworkips=myip.substring(0,i+1);
                    break;
                }
            }

            for(int i=1;i<=254;++i){
                try {
                    // Create an InetAddrss object with new IP
                    InetAddress addr=InetAddress.getByName(mynetworkips + new Integer(i).toString());
 
                    if (addr.isReachable(100)){ 
                        System.out.println("Available: " + addr.getHostAddress()); 
                        Server.IPs.add(addr.getHostAddress()); 
                    }
                    else System.out.println("Not available: "+ addr.getHostAddress());  // show that it is not available
 
                }catch (IOException ioex){
                    // nothing to do, just catch it if something goes wrong
                }
            }
        }
    }
    public void write_file(String filename) throws FileNotFoundException, IOException
    {
         OutputStream out = null;
         out = new FileOutputStream(filename);
        byte[] bytes = new byte[16*1024];

        int count;
        while ((count = input_data.read(bytes)) > 0) {
            out.write(bytes, 0, count);
        }
        out.close();
    }
    @Override
    public void run()
    {
        while(true)
        {
            String message;
            System.out.println("Wait for the Client");
            listen();
            String ip      = get_input();
            String message_type = get_input();
            if(message_type.equals("M"))
            {
                message = get_input();
                //start receive
                Chat_GUI.rec_message(message, ip);
            }
            else{
                try {
                    Chat_GUI.rec_message("File Recieved", ip);
                    this.write_file(message_type);
                    
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null,"server"+ ex);
                }
            }
             
            //end receiving
        }//end loop
    }//end thread 
}
