
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author El-Wattaneya
 */
public class UDP_Server implements Runnable
{
    private DatagramSocket socket;
    private byte[]buf = new byte [16*1024];
    public int broadcast_port = 4415;
    public UDP_Server()
    {
        try
        {
            socket = new DatagramSocket(broadcast_port);
        }//end try
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e);
        }//end catch
    }//end constructor
    @Override
    public void run() 
    {
        while(true)
        {
            try
            {
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);
                
                String packet_type = new String(packet.getData() ,0 ,packet.getLength());
                
                if(packet_type.equals("M"))
                {
                    System.out.println("in");
                    socket.receive(packet);
                    InetAddress ip = packet.getAddress();
                    //byte offset length
                    String message = new String(packet.getData(),0,packet.getLength());
                    System.out.println(message.length());
                    Chat_GUI.rec_message(message, ip.toString());
                }
                else
                {
                    socket.receive(packet);
                    InetAddress ip = packet.getAddress();
                    write_file(packet.getData() , packet_type);
                    Chat_GUI.rec_message("File Recieved BroadCast", ip.toString());
                }
                
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(null,"UDB"+ e);
            }
        }//end working thread
    }//end thread
    public void write_file(byte[] file , String filename) throws FileNotFoundException, IOException
    {
        
        OutputStream out = null;
        out = new FileOutputStream(filename);
         
         out.write(file);

        out.close();
    }
}//end class
