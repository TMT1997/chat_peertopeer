
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Talaat
 */
public class Chat {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SocketException 
    {
        // TODO code application logic here
       System.out.println("Start");
       //start tcp server
       Server server = new Server();
       Thread th_Server = new Thread(server);
       th_Server.start();
       //start gui chat
       Chat_GUI chat = new Chat_GUI();
       chat.setVisible(true);
       //start udp server
       UDP_Server udp_server = new UDP_Server();
       Thread udp_thread = new Thread(udp_server);
       udp_thread.start();
       //--------------------------------------------------------------------------------
    }
    
}
